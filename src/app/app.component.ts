import {Component} from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Chatting';

  colour = ['red', 'blue', 'green', 'yellow', 'brown'];

  listOfUsers = [{
    userName: '',
    message: ''
  }];

  listOfMessages = [{
    messages: '',
    activeUser: ''
  }];

  listOfActiveUsers = [{
    activeUser: '',
    activeMessage: ''
  }];

  newUsers = {userName: '', message: ''};
  newMessages = {messages: '', activeUser: ''};
  activeUsers = {userName: '', message: ''};

  addNewUser() {

    this.listOfUsers.push(
      {
        userName: this.newUsers.userName,
        message: this.newUsers.message
      }
    );
  }

  addNewMessage() {
    this.listOfMessages.push({
        messages: this.newMessages.messages,
        activeUser: this.newMessages.activeUser,
      }
    );
  }

}
